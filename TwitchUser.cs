﻿
namespace TwitchApi
{
    public struct TwitchUser
    {
        public override int GetHashCode()
        {
            unchecked
            {
                return ((Username != null ? Username.GetHashCode() : 0)*397) ^ IsMod.GetHashCode();
            }
        }

        public string Username { get; set; }
        public bool IsMod { get; set; }

        public TwitchUser(string username, bool isMod) : this()
        {
            Username = username;
            IsMod = isMod;
        }

        public override string ToString()
        {
            return Username;
        }

        public bool Equals(TwitchUser other)
        {
            return string.Equals(Username, other.Username) && IsMod.Equals(other.IsMod);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is TwitchUser && Equals((TwitchUser) obj);
        }

        public static bool operator ==(TwitchUser u1, TwitchUser u2)
        {
            return u1.Equals(u2);
        }

        public static bool operator !=(TwitchUser u1, TwitchUser u2)
        {
            return !u1.Equals(u2);
        }

        public static bool operator ==(TwitchUser u1, string u2)
        {
            return u1.Username.Equals(u2);
        }

        public static bool operator !=(TwitchUser u1, string u2)
        {
            return !u1.Username.Equals(u2);
        }
    }
}

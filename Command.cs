﻿using System;

namespace TwitchApi
{
    public class Command
    {
        private readonly Action<CommandArgs> _action;
        public string CmdName { get; private set; }

        public bool UsePrefix = true;

        public Command(string cmdName)
        {
            CmdName = cmdName;
        }

        /// <summary>
        /// This is used for inline commands
        /// </summary>
        /// <param name="cmdName"></param>
        /// <param name="action"></param>
        public Command(string cmdName, Action<CommandArgs> action)
        {
            _action = action;
            CmdName = cmdName;
        }

        /// <summary>
        /// Gets called when a user types the command in the chat
        /// </summary>
        /// <param name="e"></param>
        public virtual void Run(CommandArgs e)
        {
            if(_action != null)
                _action.Invoke(e);
        }
    }

    public sealed class CommandArgs : EventArgs
    {
        /// <summary>
        /// The chat instance
        /// </summary>
        public TwitchChat Chat { get; private set; }
        /// <summary>
        /// The sender of the command
        /// </summary>
        public TwitchUser Sender { get; private set; }
        /// <summary>
        /// A Array of the message, split by spaces
        /// </summary>
        public string[] Messages { get; private set; }

        /// <summary>
        /// The full message, not split
        /// </summary>
        public string Message { get; private set; }

        public CommandArgs(TwitchChat c, TwitchUser u, string[] m)
        {
            Chat = c;
            Sender = u;
            Messages = m;

            Message = string.Join(" ", Messages);
        }
    }
}

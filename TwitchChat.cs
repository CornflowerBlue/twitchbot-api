﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using TwitchApi.Events;

namespace TwitchApi
{
    public delegate void ChatMessage(TwitchChat sender, ChatMessageEventArgs e);
    public delegate void User(TwitchChat sender, TwitchUser user);
    public delegate void Connected(TwitchChat sender);

    public sealed class TwitchChat
    {
        /// <summary>
        /// When somebody in the channel sends a message
        /// </summary>
        public event ChatMessage OnChatMessage;
        /// <summary>
        /// When you join the channel
        /// </summary>
        public event Connected OnConnected;
        /// <summary>
        /// When a user joins the Channel
        /// </summary>
        public event User OnUserJoin;
        /// <summary>
        /// When a user leaves the channel
        /// </summary>
        public event User OnUserLeave;
        /// <summary>
        /// When a user Subscribes
        /// </summary>
        public event User OnUserSubscribed;

        private readonly string _oAuthKey;

        private string _lastMessage;
        private int _sentCommands;
        private readonly List<string> _buffedCommands = new List<string>();
        private readonly List<string> _modList = new List<string>();
        private readonly List<Command> _commands = new List<Command>(); 

        private TcpClient _tcpClient;
        private StreamWriter _writer;
        private StreamReader _reader;

        public bool Running { get; private set; }
        public string Username { get; private set; }
        public string Channel { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username">The username of the owner of your oAuth Key</param>
        /// <param name="oAuthKey">The oAuth key of your account</param>
        /// <param name="channel">The Channel you wish to join</param>
        public TwitchChat(string username, string oAuthKey, string channel)
        {
            Username = username.ToLower();
            Channel = channel.ToLower();
            _oAuthKey = "oauth:" + oAuthKey;
        }

        /// <summary>
        /// Connects to the channel
        /// </summary>
        public void Connect()
        {
            if (Running) return;
            Running = true;

            _modList.Clear();
            _modList.Add(Channel);

            _tcpClient = new TcpClient(TwitchSettings.IrcHost, TwitchSettings.IrcPort);
            _writer = new StreamWriter(_tcpClient.GetStream());
            _reader = new StreamReader(_tcpClient.GetStream());

            SendConnection();

            new Thread(StartReceive).Start();
            new Thread(TimeoutThread).Start();
        }

        private void TimeoutThread()
        {
            while (Running)
            {
                Thread.Sleep(31 * 1000);

                _sentCommands = 0;

                lock (_buffedCommands)
                {
                    for (var i = 0; i < Math.Min(_buffedCommands.Count, 20); i++)
                    {
                        SendCommand(_buffedCommands[0]);
                        _buffedCommands.RemoveAt(0);
                    }
                }
            }
        }

        private void SendConnection()
        {
            _writer.WriteLine("USER " + Username + "tmi twitch :" + Username);
            _writer.WriteLine("PASS " + _oAuthKey);
            _writer.WriteLine("NICK " + Username);
            _writer.WriteLine("JOIN #" + Channel);
            _writer.WriteLine("TWITCHCLIENT  1");
            _writer.Flush();
        }

        private void StartReceive()
        {
            string inputLine;
            while ((inputLine = _reader.ReadLine()) != null)
            {
                var splitInput = inputLine.Split(' ');

                if (splitInput[1] == "366")
                    RaiseOnConnected();

                else if (splitInput[1] == "MODE")
                    _modList.Add(splitInput[4]);

                else if (splitInput[1].Equals("PRIVMSG"))
                {
                    var message = splitInput.ToList();
                    message.RemoveRange(0, 3);
                    message[0] = message[0].Substring(1, message[0].Length - 1);

                    var senderUsername = splitInput[0].EndsWith("jtv")
                        ? "jtv" : splitInput[0].Substring(1, splitInput[0].IndexOf("!", StringComparison.Ordinal) - 1);
                    var sender = new TwitchUser(senderUsername, _modList.Contains(senderUsername));

                    if (!TwitchSettings.IgnoredUsers.Contains(senderUsername))
                    {
                        if (senderUsername == "twitchnotify")
                        {
                            if (message.Any(m => m.Equals("subscribed!")))
                                RaiseOnUserSubscribed(new TwitchUser(message[0], _modList.Contains(message[0].ToLower())));
                        }
                        else
                            RaiseOnChatMessage(new ChatMessageEventArgs(sender, string.Join(" ", message)));

                        // Commands
                        CheckForCommands(message, sender);
                    }
                }

                else if (splitInput[1].Equals("JOIN"))
                {
                    var sender = splitInput[0].Substring(1, splitInput[0].IndexOf("!", StringComparison.Ordinal) - 1);
                    RaiseOnUserJoin(new TwitchUser(sender, _modList.Contains(sender.ToLower())));
                }

                else if (splitInput[1].Equals("PART"))
                {
                    var sender = splitInput[0].Substring(1, splitInput[0].IndexOf("!", StringComparison.Ordinal) - 1);

                    RaiseOnUserLeave(new TwitchUser(sender, _modList.Contains(sender.ToLower())));
                }
            }
        }

        private void CheckForCommands(List<string> message, TwitchUser sender)
        {
            if (message[0] == string.Empty) return;
            var cmd = message[0].ToLower();
            message.RemoveAt(0);

            foreach (var c in _commands.Where(c => cmd == (c.UsePrefix ? TwitchSettings.DefaultCommandPrefix.ToString() : "") + c.CmdName.ToLower()))
            {
                try
                {
                    c.Run(new CommandArgs(this, sender, message.ToArray()));
                }
                catch
                {
                    // ignored
                }
            }
        }

        private void RaiseOnChatMessage(ChatMessageEventArgs e)
        {
            var handler = OnChatMessage;
            if (handler != null) handler(this, e);
        }

        private void RaiseOnUserJoin(TwitchUser e)
        {
            var handler = OnUserJoin;
            if (handler != null) handler(this, e);
        }

        private void RaiseOnUserLeave(TwitchUser e)
        {
            var handler = OnUserLeave;
            if (handler != null) handler(this, e);
        }

        private void RaiseOnUserSubscribed(TwitchUser user)
        {
            var handler = OnUserSubscribed;
            if (handler != null) handler(this, user);
        }

        private void RaiseOnConnected()
        {
            var handler = OnConnected;
            if (handler != null) handler(this);
        }

        /// <summary>
        /// Send a RAW IRC command
        /// </summary>
        /// <param name="command">The RAW IRC command</param>
        /// <param name="bufer">If sending this command will cause a IP ban, it will buffer it and wait till it can send</param>
        public void SendCommand(string command, bool bufer = true)
        {
            if (_sentCommands >= 19)
            {
                if (!bufer) return;

                lock (_buffedCommands)
                    _buffedCommands.Add(command);
                return;
            }

            _sentCommands++;

            _writer.WriteLine(command);
            _writer.Flush();
        }

        /// <summary>
        /// Send a Twitch Chat message, this cant be used for Chat Commands such as /ban, use SendRaw instead
        /// </summary>
        /// <param name="message">The message you want to send</param>
        public void SendMessage(string message)
        {
            if (_lastMessage == message)
                message = TwitchSettings.DuplicateMessagePrefix + " " + message;

            _lastMessage = message;
            SendCommand(string.Format("PRIVMSG #{0} : {1}", Channel, message), false);
        }

        /// <summary>
        /// Add a command to the system using the built in command system, the default prefix is '$' this can be changed in TwitchSettings.DefaultCommandPrefix
        /// </summary>
        /// <typeparam name="T">Return Type</typeparam>
        /// <param name="command">The instance of the command</param>
        /// <returns></returns>
        public T AddCommand<T>(T command) where T : Command
        {
            if (!_commands.Contains(command))
                _commands.Add(command);
            return command;
        }

        /// <summary>
        /// This removes a command from the system
        /// </summary>
        /// <param name="command">The reference to the Command object</param>
        /// <returns>If removale was successful</returns>
        public bool RemoveCommand(Command command)
        {
            var r = _commands.Contains(command);
            if(r) _commands.Remove(command);

            return r;
        }

        public TwitchUser[] GetModList()
        {
            lock (_modList)
            {
                var mods = new TwitchUser[_modList.Count];

                for (var i = 0; i < _modList.Count; i++)
                    mods[i] = new TwitchUser(_modList[i], true);

                return mods.ToArray();
            }
        }
    }
}

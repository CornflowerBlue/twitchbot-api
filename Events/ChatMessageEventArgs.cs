﻿using System;

namespace TwitchApi.Events
{
    public sealed class ChatMessageEventArgs : EventArgs
    {
        public TwitchUser Sender { get; set; }
        public string Message { get; set; }

        public ChatMessageEventArgs(TwitchUser sender, string message)
        {
            Sender = sender;
            Message = message;
        }

        public override string ToString()
        {
            return string.Format("[{0}] {1}", Sender.Username, Message);
        }
    }
}

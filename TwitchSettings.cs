﻿using System.Collections.Generic;

namespace TwitchApi
{
    public static class TwitchSettings
    {
        public static char DefaultCommandPrefix = '$';
        /// <summary>
        /// If a message is the same as the last same message it wont be sent, so we add this prefix so the message will be different
        /// </summary>
        public static char DuplicateMessagePrefix = '+';
        /// <summary>
        /// Any users in this list will be blacklisted from the Message Event, and all commands
        /// </summary>
        public static List<string> IgnoredUsers = new List<string> { "jtv" };

        public static string IrcHost = "irc.twitch.tv";
        public static ushort IrcPort = 6667;

    }
}

Example usage;

```
#!c#

var t = new TwitchChat("Bot Twitch Username", "oAuth Key - without 'oAuth:'", "Channel to join"); // The command prefix is '$', example "$potatoes"
t.OnChatMessage += t_OnChatMessage;
t.OnUserJoin += t_OnUserJoin;
t.OnUserLeave += t_OnUserLeave;
t.OnConnected += t_OnConnected;

// Both of these methods will work, it's recommended to just stick with classes
t.AddCommand(new ExampleCommand()); // You can use a dedicated class, this way is cleaner
t.AddCommand(new Command("potatoes", e => e.Chat.SendMessage("CORN!"))); // Or you can use it this way, this way is faster (To type)

t.Connect();
```
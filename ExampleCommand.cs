﻿namespace TwitchApi
{
    class ExampleCommand : Command
    {
        public ExampleCommand() : base("test") // this string is what people will say in chat, example "$test"
        {
            
        }

        // this gets called when somebody sends this command
        public override void Run(CommandArgs e)
        {
            base.Run(e);

            // in the events args, you can get the sender, message, and message split by spaces.
            // You also have a reference to the chat so you can send feedback
        }
    }
}
